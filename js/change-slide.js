function changeSlideMix(obj,block,li)
{
	$(obj).parent().find(block+' '+li).stop(true,true);
	if($(obj).hasClass('slider-prevmix'))
	{
		liLast=$(obj).parent().find(block+' '+li+':last');
		liLast.prependTo(liLast.parent()).css('margin-left','-'+liLast.width()+'px').animate({marginLeft:0});		
		var currImg = liLast.find('img').attr('src');
		$('#innerSlide img').remove();
		$('#innerSlide').prepend('<img class="innerLi" src="' + currImg + '"></img>');
	}
	else {
		liFirst=$(obj).parent().find(block+' '+li+':eq(0)');
		liFirst.animate({marginLeft:0-liFirst.width()},function(){
		liFirst.css('margin-left',0).appendTo(liFirst.parent());
		}); 
		var currImg = liFirst.find('img').attr('src');
		$('#innerSlide img').remove();
		$('#innerSlide').prepend('<img class="innerLi" src="' + currImg + '"></img>');
	} 
}

$(document).ready(function(){
	setInterval(function(){changeSlideMix('.slider-nextmix', '.horizontal-slidermix','.slide-itemmix')}, 4000);
});