$( document ).ready( function(){
            var entries = [ 
                { label: 'Crime', url: ''},
                { label: 'Finance', url: ''},
                { label: 'Strange', url: ''},
                { label: 'Disasters', url: ''},
                { label: 'Science', url: ''},
                { label: 'Terrorism', url: ''},
                { label: 'Education', url: ''},
                { label: 'Millitary', url: ''},
                { label: 'Economics and Business', url: ''},
                { label: 'Electronics', url: ''},
                { label: 'Technology', url: ''},
                { label: 'Sport', url: ''},
                { label: 'Health', url: ''},
                { label: 'Lawsuits and investigations', url: ''},
                { label: 'Civil rights', url: ''}
            ];
            var settings = {
                entries: entries,
                width: 1800,
                height: 1000,
                radius: '80%',
                radiusMin: 10,
                bgDraw: false,
                bgColor: '#fff',
                opacityOver: 1.00,
                opacityOut: 0.05,
                opacitySpeed: 6,
                fov: 800,
                speed: 0.4,
                fontFamily: 'Oswald, Arial, sans-serif',
                fontSize: '2.074em',
                fontColor: 'rgba(51, 51, 51, 0.302)',
                fontWeight: 'normal',//bold
                fontStyle: 'normal',//italic 
                fontStretch: 'normal',//wider, narrower, ultra-condensed, extra-condensed, condensed, semi-condensed, semi-expanded, expanded, extra-expanded, ultra-expanded
                fontToUpperCase: false
            };
            var svg3DTagCloud = new SVG3DTagCloud( document.getElementById('tag-cloud'), settings );
            //$('#tag-cloud').svg3DTagCloud(settings);
} );
        