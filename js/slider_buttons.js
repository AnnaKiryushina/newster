$(document).ready(function(){

    $(window).resize(function(){
        $('.information').height($('.information').width()/2.3);
    });

    
    $(".slide").each(function(i){
        $(this).attr('id','Slide-' + i);
        //$("#indexActive").prepend('<img id="active_Ind_'+i+'" class="sl-btn" src="img/slide' + i + '.jpg"></img>');
        if(i > 0)
        {
            $("#indexActive").prepend('<img class="sl-btn" src="img/slide' + i + '.jpg"></img>');
        }
    }); 

    var toTheLeft;
    $(".sl-btn").each(function(i){
        $(this).attr('id', 'active_Ind_' + i);
        toTheLeft = i * 180;
        $(this).css({left: toTheLeft});
    }); 

    $("#indexActive #active_Ind_0").addClass('active');
    $("#slideWrp #Slide-0").addClass('activeInner');
    $("#slideWrp").prepend('<div class="slide"><img class="slide__img" src="img/slide0.jpg"></img></div>');
    $(".slide #Slide-0").appendTo('#slideWrp');

    var SLlength = $(".slide").length - 1;
    var currentIndex = 0;
    var timeout;
    var $group = $('.directional_nav');
    var $nav_btns = $('.nav-btns');
    var $sl_btn = $('.sl-btn');
    var bulletArray = [];

    function move(newIndex) 
    {
        
        //advance();
        alert(newIndex);

        $("#slideWrp").prepend('<img class="slide__img" src="img/slide' + newIndex + '.jpg"></img>');
        $(".slide #Slide-" + newIndex).appendTo('#slideWrp');
        $("#indexActive #active_Ind_" + newIndex).remove();
        if(newIndex < SLlength)
        {
            var currPos = $("#indexActive #active_Ind_" + (newIndex + 1)).position();
            alert(currPos.left);
        }

        var animateLeft, slideLeft;

        if (newIndex > currentIndex) 
        {
            slideLeft = '160px';
            animateLeft = '-160px';
        } 
        else 
        {
            slideLeft = '-160px';
            animateLeft = '160px';
        }

        $sl_btn.eq(newIndex).css({
            display: 'block',
            right: slideLeft
        });

        currentIndex = newIndex;

        /*$nav_btns.animate({
            left: animateLeft
            }, function() {
            $nav_btns.eq(currentIndex).css({
                display: 'none'
            });
        
            $nav_btns.eq(newIndex).css({
                left: 0
            });
        
            $nav_btns.css({
                left: 0
            });
        
            currentIndex = newIndex;
        });
        */

        /*var prevActive = +$("#indexActive > .sl-btn.active").attr('id').replace('active_Ind_','');
        $("#indexActive > .sl-btn.active").removeClass('active');
        $("#indexActive #active_Ind_"+newIndex).addClass('active');

        $("#slideWrp > .slide.activeInner").removeClass('activeInner');
        $("#slideWrp #Slide-"+newIndex).addClass('activeInner');
        var innerSlide = $(".slide #Slide-"+newIndex);
        innerSlide.appendTo('#slideWrp');

        var slideToShow = $(".slide:first");
        slideToShow.appendTo('#slideWrp');
        slideToShow = innerSlide;*/

    }

    function advance() 
    {
        clearTimeout(timeout);
        timeout = setTimeout(function() {
            if (currentIndex < (SLlength - 1)) 
            {
                move(currentIndex + 1);
            } 
            else 
            {
                move(0);
            }
        }, 4000);
    }

//нажатие кнопки для перехода на следующий слайд
    $('.next_btn').on('click', function() 
    {
        if (currentIndex < (SLlength - 1)) 
        {
            move(currentIndex + 1);
        } 
        else 
        {
            move(0);
        }
    });

//нажатие кнопки для перехода на предыдущий слайд
    $('.previous_btn').on('click', function() 
    {
        if (currentIndex !== 0) 
        {
            move(currentIndex - 1);
        } 
        else 
        {
            move(SLlength - 1);
        }
    });   

});
