$(document).ready(function(){

	function rotateText(id)
	{
		var imgHeight = $('.rot-img' + id).height();
		//alert("h = " + $('.rot-img' + id).height() + ", w = " + $('.rot-img' + id).width());
		$(".fs" + id).attr('width', imgHeight);
		$(".fs" + id).attr('height', "20px");
	}

	var rotatedConetent = $(this).find('.related__content'); //Получены все div с классом .related__content
	var rotatedConetent_length = rotatedConetent.length;
	
	for(var i = 0; i < rotatedConetent_length; i++) //присвоены классы для изображения и повернутого текста
	{
		var currRotatedConetent = rotatedConetent[i];
		var nonRotatedImgs = $(currRotatedConetent).find('.related__content-img');
		var fieldSet = $(currRotatedConetent).find('fieldset');
		nonRotatedImgs.addClass('rot-img' + i);
		fieldSet.addClass('fs' + i);
	}

	for(var i = 0; i < rotatedConetent_length; i++)
	{
		rotateText(i);
		$(".fs" + i).css({
			"border-top": "2px groove $blackColor !important",
			"border-right": "2px solid $blackColor",
    	    "border-left": "2px solid $blackColor",
    	    "border-bottom": "none",
    	   	"padding": "0 0 1.4em 0",
    		"margin-top": "0",
    		"margin-bottom": "0", //"1.5em",
    		"margin-right": "0",
    		"margin-left": "0",
    		"transform": "rotate(90deg)",
    		"-ms-transform": "rotate(90deg)",
    		"-moz-transform": "rotate(90deg)",
    		"-webkit-transform": "rotate(90deg)", 
    		"-ms-transform-origin": "left top 0",
    		"-moz-transform-origin": "left top 0",
    		"-webkit-transform-origin": "right top 0",
    		"transform-origin": "right top 0"  
    	});
	}
	$('.table__legend').css({"font-size": "10px !important"});
	//alert("hfs0 = " + $('.fs0').height() + ", wfs0 = " + $('.fs0').width());
});